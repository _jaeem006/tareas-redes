#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <arpa/inet.h>

#define MAX 1024
#define SA struct sockaddr

void comunicacion(int sockfd)
{
  char buff[MAX];
  int n;
  while(1) {
    bzero(buff, sizeof(buff));
    n = 0;
    while ((buff[n++] = getchar()) != '\n');
    buff[strlen(buff)-1]='\0';    
    if (strncmp("EOF", buff, 3) == 0) { break; }
    write(sockfd, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
    read(sockfd, buff, sizeof(buff));
    printf("%s\n", buff);
  }
}

int main(int argc, char **argv)
{
  int sockfd, connfd;
  struct sockaddr_in servaddr, cli;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    printf("Error en creación de socket\n");
    exit(1);
  }
  bzero(&servaddr, sizeof(servaddr));

  if(argc != 3) {
    printf("# incorrecto de argumentos\n");
    exit(1);
  }
  int port = atoi(argv[2]);

  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(argv[1]);
  servaddr.sin_port = htons(port);

  if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
    printf("Conexión fallida con el servidor\n");
    exit(1);
  }
  printf("Para terminar la conexión escribe 'EOF'\n");
  comunicacion(sockfd);

}
