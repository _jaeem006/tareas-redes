#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

// metodo que agrega un elemento al final de la lista
void agrega_elemento(struct lista* lista, void* elemento){

	// definimos nuevo, el nodo a agregar
	struct nodo* nuevo = (struct nodo*)malloc(sizeof(struct nodo));
	//guardamos el valor
	nuevo -> elemento = elemento;
	nuevo -> siguiente = NULL;

	//Caso para la lista vacía
	if (lista -> longitud == 0)
		lista -> cabeza = nuevo;
	//Cuando la lista no es vacia
	else{
		//necesitamos un nodo auxiliar que vaya iterando sobre la lista
		struct nodo* nodo = lista -> cabeza;

		for (int i = 0; i < lista->longitud - 1; i++)
			nodo = nodo -> siguiente;

		nodo -> siguiente = nuevo;
	}

	//aumentamos la longitud de la lista.
	lista -> longitud++;

}

// método para obtener el n-esimo elemento de una lista
void* obten_elemento(struct lista* lista, int n){
	// nodo auxiliar para ir recorriendo la lista
	struct nodo* nodo = lista->cabeza;

	// para almacenar el elemento una vez que lo encontremos
	void* elem;

	for (int i = 0; i < lista -> longitud; i++){
		// Si ya llegue al elemento n-esimo
		if(n == i){
			elem = nodo -> elemento;
			break;
		}
		// sino, sigo iterando.
		nodo = nodo->siguiente;
	}

	return elem;
}

// para eliminar el n-ésimo elemento de la lista
void* elimina_elemento(struct lista* lista, int n){

	// Nodo para iterar sobre la lista
	struct nodo* nodo = lista -> cabeza;
	struct nodo* sig;
	// apuntador para el regreso
	void* elem;

	// manejamos el caso en el que la lista no tiene n elementos
	if (lista -> longitud < n)
    	return elem;

    // Si queremos eliminar la cabeza
	if(n == 0){
		// la cabeza d ela lista ahora será el segundo elemento.
		lista -> cabeza = nodo -> siguiente;
		elem = nodo -> elemento;
		// decrementamos la longitu de la lista
		lista -> longitud--;
		// liberamos la memoria
		free(nodo);
	}
	// en otro caso
	else{
		//buscmos el elemento a eliminar
		for (int i = 0; i < lista -> longitud; i++){
			// buscamos el elemento anterior para cambiar la referencia de siguiente.
			if((n - 1) == i){
				// 
				sig = nodo -> siguiente;
				//guardamos el elemento para regresarlo
				elem = sig -> elemento;
				// apuntamos siguiente al siguiente del nodo que eliminaremos
				nodo -> siguiente = (sig -> siguiente);
				// disminuimos la longitud
				lista -> longitud--;
				// Liberamos memoria
				free(sig);
				break;
			}
			// Si no es el anterior, seguimos buscando.
			nodo = nodo -> siguiente;
		}
	}
	return elem;
}

// funciona como map.
void aplica_funcion(struct lista* lista, void (*f)(void*)){
	// Nodo para iterar la lista 
	struct nodo* nodo = lista -> cabeza;
	//iteramos sobre la lista 
	for (int i = 0; i < lista -> longitud; i++){
		// aplicamos la función al elemento 
		f(nodo -> elemento);
		//nos movemos al nodo siguiente.
		nodo = nodo -> siguiente;
	}
}
