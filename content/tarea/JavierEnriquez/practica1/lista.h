// estructura nodo
struct nodo {
	void* elemento;
	struct nodo* siguiente;
};

// estructura lista 
struct lista {
	struct nodo* cabeza;
	int longitud;
};

// Funciones definidas para la estructura
void agrega_elemento(struct lista* lista, void* elemento);

void* obten_elemento(struct lista* lista, int n);

void* elimina_elemento(struct lista* lista, int n);

void aplica_funcion(struct lista* lista, void (*f)(void*));
