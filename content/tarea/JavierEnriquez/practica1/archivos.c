#include "lista.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void agregar(struct lista *l);
void leer(struct lista *l);
void eliminar(struct lista *l);
void imprimir(struct lista *l);

// metodo para almacenar el nombre de un archivo en una lista 
void agregar(struct lista *l){

  char fname[51] = "";

  printf("Nombre del archivo:\n");

  scanf("%50s", fname);
  fflush(stdin);

  if( access(fname, F_OK) != -1 ){

    char *p = (char *)malloc(sizeof(char) * strlen(fname));
    strcpy(p, fname);
    agrega_elemento(l, p);
    printf("Se agrego el archivo %s \n", fname);

  }else
    fprintf(stderr, "No se encontro el archivo %s \n", fname);

}

// auxiliar para imprimir cada entrada de la lista
void imprimeNombre(void* elemento){
	char* e = (char*)elemento;
	printf("%s\n", e);
}

//Método para imprimir un archivo
void imprimir(struct lista* l){
	if(l -> longitud == 0)
		fprintf(stderr,"No hay archivos.\n"); 
	else{
		printf("La lista tiene los archivos:\n");
		aplica_funcion(l,imprimeNombre);
	}
}

// Metodo para leer el archivo n-ésimo de la lista
void leer(struct lista *l){

  int input;

  printf("Indece del archivo a leer:\n");

  scanf("%d", &input);
  fflush(stdin);

  if(l -> longitud < input){
    fprintf(stderr, "No existe el archivo %d\n", input);
  } else{
    for(int i = 0; i < l -> longitud; i++){
      if(i == input){
        char *fname = (char *)obten_elemento(l, input);
        FILE *fptr = fopen(fname, "r");
        if(fptr == NULL){
          fprintf(stderr, "Error al leer el archivo %s\n", fname);
          return;
        }
        char c = fgetc(fptr);
        while(c != EOF){
          printf("%c", c);
          c = fgetc(fptr);
        }

        fclose(fptr);
      }
    }
  }
}

//metodo para eliminar archivos de una lista
void eliminar(struct lista *l){

  int input;

  printf("Indice del archivo a eliminar:\n");

  scanf("%d", &input);
  fflush(stdin);

  if(l -> longitud < input){
    fprintf(stderr, "No existe el archivo %d\n", input);
  } else{
    elimina_elemento(l, input);
    printf("Se elimino el archivo %d\n", input);
  }

}


int main(){
  struct lista l = {cabeza : 0, longitud : 0};
  //Para almacenar la selección del usuario.
  int input;
  for(;;){

    // imprimimos las opciones
    printf("Selecciona una opción \n [1] Insertar archivo\n [2] Leer archivo\n [3] Eliminar archivo\n [4] Imprimir archivo\n [5] Salir\n");

    // vemos la entrada del usuario
    scanf("%d", &input);
    fflush(stdin);

    switch(input){
      case 1:
        agregar(&l);
        break;
      case 2:
        leer(&l);
        break;
      case 3:
        eliminar(&l);
        break;
      case 4:
        imprimir(&l);
        break;
      case 5:
        return 0;
      default:
        printf("Esa no es una opción valida.\n");
    }
  }
  return 0;
}
