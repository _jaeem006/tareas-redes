# Facultad de Ciencias UNAM 
## Redes de computadora
## Tarea HTTP
### Javier Enríquez Mendoza

#### 5. Comparar archivos de salida y obtener conclusiones

#### ¿Existen diferencias entre los archivos?, ¿Cuáles?

Las diferencias s encuentran en el archivo diferencias.txt

#### ¿Cambia la respuesta del servidor cuando se pasa la cookie en la petición?

Si pues almacenan información cifrada para evitar que los datos almacenados en ellas sean vulnerables a ataques maliciosos de terceros.

#### ¿Por qué se envian las cookies en la petición al servidor?

Para decirle al servidor que dos peticiones tienen su origen en el mismo navegador web lo que permite, por ejemplo, mantener la sesión de un usuario abierta.

#### ¿Por qué se quitó la opción --compressed en la línea de comandos de curl?

Para que la respuesta del servidor no estuviera comprimida y fuera legible para nosotros.

#### ¿Por qué se alteró el valor de la cabecera Accept que se mandó en la petición?

Para que sólo recibieramos html.




