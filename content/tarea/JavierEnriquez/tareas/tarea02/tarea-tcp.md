# Facultad de Ciencias UNAM 
## Redes de computadora
## Tarea TCP
### Javier Enríquez Mendoza

#### 1. Verificar que el dominio tenga el servicio de HTTP

$ cat dominio.txt
uglychristmassweater.com

$ dig +short www.uglychristmassweater.com | tee ip.txt
uglysweater.wpengine.com.
35.202.153.252

$nc -vz www.uglychristmassweater.com 80
found 0 associations
found 1 connections:
     1:	flags=82<CONNECTED,PREFERRED>
	outif en0
	src 10.20.30.159 port 65316
	dst 35.202.153.252 port 80
	rank info not available
	TCP aux info available

Connection to www.uglychristmassweater.com port 80 [tcp/http] succeeded!

#### 2. Captura de tráfico utilizando tcpdump

$ tcpdump -n -w www_uglycristmassweater_com.pcap 'host www.uglychristmassweater.com'
tcpdump: data link type PKTAP
tcpdump: listening on pktap, link-type PKTAP (Apple DLT_PKTAP), capture size 262144 bytes
10 packets captured
2843 packets received by filter
0 packets dropped by kernel

#### 3. Petición HTTP

$ curl -v# 'http://www.uglychristmassweater.com/' -o www_uglychristmassweater_com.http.html 2>&1 | tr -d '\r' | tee www_uglychristmassweater_com.http.log
*   Trying 35.202.153.252...
* TCP_NODELAY set
* Connected to www.uglychristmassweater.com (35.202.153.252) port 80 (#0)
> GET / HTTP/1.1
> Host: www.uglychristmassweater.com
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 301 Moved Permanently
< Server: nginx
< Date: Fri, 12 Apr 2019 19:58:01 GMT
< Content-Type: text/html
< Content-Length: 178
< Connection: keep-alive
< Keep-Alive: timeout=20
< Location: https://www.uglychristmassweater.com/
< X-Type: default
<
{ [178 bytes data]
######################################################################## 100.0%* Connection #0 to host www.uglychristmassweater.com left intact

##### Diagrama

Cliente                                                                 Servidor
10.20.30.159.65389                                             35.202.153.252.80

 |                                                                            |
 |  1: Flags [SEW] , seq 1152233187 , win 65535 , length 0                    |
 |  ------------------------------------------------------------------------->|
 |                                                                            |
 |  2:   Flags [S.E] , seq 2069197607 , ack 1152233188 , win 28400 , length 0 |
 |<---------------------------------------------------------------------------|
 |                                                                            |
 |  3: Flags [.]  , ack 1 , win 4096 , length 0                               |
 |  ------------------------------------------------------------------------->|
 |                                                                            |
 |  4: Flags [P.] , seq 1:93 , ack 1 , win 4096 , length 92                   |
 |     GET / HTTP 1.1                                                         |
 |  ------------------------------------------------------------------------->|
 |                                                                            |
 |  5:                        Flags [.] , seq 1 , ack 93 , win 56 ,  length 0 |
 |<---------------------------------------------------------------------------|
 |                                                                            |
 |  6:                 Flags [P.] , seq 1:425 , ack 93 , win 56 , length 424  |
 |                                             HTTP/1.1 301 Moved Permanently |
 |<---------------------------------------------------------------------------|
 |                                                                            |
 |  7: Flags [.] , ack 425, win 4089, length 0                                |
 |  ------------------------------------------------------------------------->|
 |                                                                            |
 |  8: Flags [F.] , seq 93, ack 425, win 4096, length 0                       |
 |  ------------------------------------------------------------------------->|
 |                                                                            |
 |  9:                     Flags [F.] , seq 425, ack 94, win 56, length 0.    |
 |<---------------------------------------------------------------------------|
 |                                                                            |
 | 10: Flags [.]ack 426, win 4096, length 0                                   |
 |  ------------------------------------------------------------------------->|
 |                                                                            |
 v                                                                            v


#### 4. Cerrar la captura de tráfico y visualizar el resultado
^C 
10 packets captured
2843 packets received by filter
0 packets dropped by kernel



