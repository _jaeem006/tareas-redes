# Facultad de Ciencias UNAM 
## Redes de computadora
## Tarea SSL
### Javier Enríquez Mendoza

#### Mencionar cómo es que se realiza la validación de los certificados y qué función realizan la ruta /etc/ssl/certs y el archivo chain.pem

La validación consta de distintos pasos :

- Primero se genera una certificate chain desde el certificado suministrado hasta la raiz CA. 
- Se busca el issuers certificate. La forma de encontrarlo depende de la versión de OpenSSL.
-- Primero se busca en la lista de los certificados "untrusted" si no se encuentra entonces comienza a buscar en los "trusted"
-- La raiz CA siempre se encuentra en los certificados "trusted"
- Se revisan todos los certificados de la lista de "untrusted" verificando la consistencia con el proposito.
- Se revisa la configutacion confiable en la raiz CA.
- se evisa la validez de la certificate chain

Si se completan todas estas peraciones, entonce el certificado es válido. Si alguna falla entonces no lo es.
