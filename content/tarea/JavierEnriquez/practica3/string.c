#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 100

void concat(char* str, char c){
    int len = strlen(str);
    str[len] = c;
    str[len + 1] = '\0';
}

int main(int argc, char *argv[]){
	FILE* f;
	int byte;
	char buffer[BUFFER_SIZE+2];

	if((f = fopen(argv[1], "rb")) == NULL){
	    printf( "error al abrir el archivo");
	    return -1;
	}

	bzero (buffer, sizeof(buffer));
	buffer[0] = '\0';

	while ((byte = getc(f)) != EOF) {
		if(32 <= byte && byte <= 126)
			concat(buffer,byte);
		else{
			if(strlen(buffer) > 1)
				printf("%s\n",buffer);
			bzero (buffer, sizeof(buffer));
			buffer[0] = '\0';
		}
	}

	printf("%s\n", buffer);

}