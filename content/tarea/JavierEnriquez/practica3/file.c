#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SIZE 10

int main(int argc, char *argv[]){
	FILE* f;
	unsigned char buffer[BUFF_SIZE];

	for (int i = 1; i < argc; i++){

		if ((f = fopen(argv[i], "r"))){

		    fread(buffer,1,BUFF_SIZE,f);

		    switch(buffer[0]){
		    	case 0x25:
		    		if(buffer[1] == 0x50 && buffer[2] == 0x44 && buffer[3] == 0x46 && buffer[4] == 0x2D){
		    			printf("Archivo PDF\n");
		    			break;
		    		}
		    	case 0x47:
		    		if (buffer[1] == 0x49 && buffer[2] == 0x46 && buffer[3] == 0x38 && buffer[5] == 0x61){
		    			printf("Archivo GIF\n");
		    			break;
		    		}
		    	case 0x50:
		    		if(buffer[2] == 0x4B){
		    			printf("Archivo ZIP\n");
		    			break;
		    		}
		    	case 0x89:
		    		if(buffer[1] == 0x50 && buffer[2] == 0x4E && buffer[3] == 0x47 && buffer[4] == 0x0D && buffer[5] == 0x0A && buffer[6] == 0x1A && buffer[7] == 0x0A){
		    			printf("Archivo PNG\n");
		    			break;
		    		}
		    	case 0x4D:
		    		if(buffer[1]==0x51){
		    			printf("Archivo EXE\n");
		    			break;
		    		}
		    	case 0x7F:
		    		if(buffer[1] == 0x45 && buffer[2] == 0x43 && buffer[3] == 0x4C && buffer[4] == 0x46){
			    		printf("Archivo ELF\n");
			    		break;
		    		}
		    	case 0xFF:
		    		if(buffer[1] == 0xFB){
		    			printf("Archivo MP3\n");
		    			break;
		    		}
		    	default:
		    		printf("No se reconocio el formato :( \n");
		    }
		}	
	}	
}

