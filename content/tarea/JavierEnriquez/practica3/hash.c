#include <openssl/md5.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void md5(char *name){

    unsigned char c[MD5_DIGEST_LENGTH];
    FILE *f;
    MD5_CTX con;
    int bytes;
    unsigned char cd[1024];

    if((f = fopen (name, "rb")) != NULL) {
        MD5_Init (&con);
        while((bytes = fread (cd, 1, 1024, f)) != 0)
            MD5_Update(&con, cd, bytes);
        MD5_Final(c, &con);
        for(int i = 0; i < MD5_DIGEST_LENGTH; i++) 
            printf("%02x", c[i]);
        fclose (f);

    }
}

void sha1(char *name){

    unsigned char c[SHA_DIGEST_LENGTH];
    FILE *f;
    SHA_CTX con;
    int bytes;
    unsigned char cd[1024];

    if((f = fopen (name, "rb")) != NULL) {

        SHA1_Init (&con);
        while((bytes = fread (cd, 1, 1024, f)) != 0)
            SHA1_Update (&con, cd, bytes);
        SHA1_Final (c, &con);
        for(int i = 0; i < SHA_DIGEST_LENGTH; i++)
            printf("%02x", c[i]);
        fclose (f);
    }
}

void sha256(char *name){

    unsigned char c[SHA256_DIGEST_LENGTH];
    FILE *f;
    SHA256_CTX con;
    int bytes;
    unsigned char cd[1024];

    if((f = fopen (name, "rb")) != NULL) {

        SHA256_Init (&con);
        while ((bytes = fread (cd, 1, 1024, f)) != 0)
            SHA256_Update (&con, cd, bytes);
        SHA256_Final (c,&con);
        for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) 
            printf("%02x", c[i]);
        fclose (f);
    }
}

int main(int argc, char *argv[]) {
        if(strcmp(argv[1], "md5") == 0)
            for(int i = 2; i < argc; i++)
                md5(argv[i]);
        else if(strcmp(argv[1], "sha1") == 0)
            for(int i = 2; i < argc; i++)
                sha1(argv[i]);
        else if(strcmp(argv[1], "sha256") == 0)
            for(int i = 2; i < argc; i++)
                sha256(argv[i]);
}
