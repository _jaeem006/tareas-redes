#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>


int main(int argc, char *argv[]){

  int sock, newsock, puerto;
  socklen_t clilen;
  char input[512] = {0};
  char output[512] = {0};
  char command[20] = {0};
  struct sockaddr_in serv_addr, cli_addr;
  char str[INET_ADDRSTRLEN];

  FILE *fp;
  if (argc < 2) {
    perror("Faltan argumentos\n");
    exit(0);
   }

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    perror("Error al crear el socket\n");
    exit(0);
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  puerto = atoi(argv[1]);

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(puerto);

  if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    perror("No se pudo conectar\n");
    exit(0);
  }

  if (listen(sock, 3) < 0){
    perror("No se pudo escuchar\n");
    exit(0);
  }

  clilen = sizeof(cli_addr);
  newsock = accept(sock, (struct sockaddr *) &cli_addr, &clilen);
  inet_ntop(AF_INET,&(cli_addr.sin_addr),str,INET_ADDRSTRLEN);

  for(;;){

    bzero(input,sizeof(input));
		bzero(output,sizeof(output));
		bzero(command,sizeof(command));
		read(newsock, input, sizeof(input));

		if(strcmp(input, "EOF") == 0)
      break;

		fp = popen(input,"r");

    while(fgets(command, sizeof(command), fp) != NULL)
      strcat(output,command);
		pclose(fp);

		write(newsock,output,strlen(output));
	}

	close(newsock);
	printf("Se ha cerrado la conexión\n");

  return 0;
}
