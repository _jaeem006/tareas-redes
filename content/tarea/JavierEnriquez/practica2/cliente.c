#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h> 
#include <unistd.h>

int main(int argc, char *argv[]){
	int cliente, puerto, dir, pret, n;
	struct sockaddr_in serv_addr;
	char input[512] = {0};
	char command[512] = {0};

	if(argc < 3){
		perror("Faltan argumentos.");
		exit(0);
	}

	cliente = socket(AF_INET, SOCK_STREAM, 0);

	if(cliente < 0){
		perror("error al abrir el socket");
		exit(0);
	}

	memset(input, '0',sizeof(input));
	puerto = atoi(argv[2]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(puerto);

	pret = inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);

	if(pret == 0){
		perror("IP incorrecta");
		exit(0);
	}

	if(pret == -1){
		perror("error con la IP");
		exit(0);
	}

	if(connect(cliente, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
		perror("error con el servidor");
		exit(0);
	}

	printf("Desea terminar la conexión [s]\n");

	while(strcmp("Si",command) != 0){
		bzero(command, sizeof(command));
		bzero(input, sizeof(input));
		fgets(command, 100, stdin);
		if( command[strlen(command) - 1] == '\n')
    		command[strlen(command) - 1] = 0;
		write(cliente, command, strlen(command));
		read(cliente, input, sizeof(input) - 1);
		printf("%s",input);
	}

	printf("Se cerro la conexión\n");
    return 0;

}
