# include<stdio.h>
# include<stdlib.h>
# include<sys/socket.h>
# include<netinet/in.h>
# include<arpa/inet.h>
# include<unistd.h>
#include <string.h>
void error ( char * mensaje ) {
  fprintf ( stderr , " %s \n " , mensaje );
  exit (1);
}
int main ( int argc , char * argv []) {
  if ( argc < 2) {
    fprintf ( stderr , " Uso : %s < puerto >\n " , argv [0]);
    return 1;
  }
  int sockfd , cliente_sockfd , opt = 1;
  struct sockaddr_in host_addr , cliente_addr;
  socklen_t sin_tam = sizeof ( struct sockaddr_in );
  char msg [256];
  if (( sockfd = socket ( AF_INET , SOCK_STREAM , 0)) < 0){
    error ( " Error al crear el socket " );
  }
  if ( setsockopt ( sockfd , SOL_SOCKET , SO_REUSEADDR , &opt , sizeof ( int )) < 0){
    error ( " Error en setsockopt \n " );
  }
  host_addr.sin_family = AF_INET ;
  host_addr.sin_port = htons ( atoi ( argv [1]));
  host_addr.sin_addr.s_addr = INADDR_ANY ;
  if ( bind ( sockfd , ( struct sockaddr *)&host_addr ,sizeof ( struct sockaddr )) < 0){
    error ( " Error en bind \n " );
  }
  if ( listen ( sockfd , 5) < 0){
    error ( " Error en listen \n " );
  }
  fflush(stdin);
  int salir = 1;
  while (salir) {

    cliente_sockfd = accept ( sockfd , ( struct sockaddr *)&cliente_addr , &sin_tam );
    if ( cliente_sockfd < 0){
      error ( " Error en accept \n " );
    }
    printf ( "Conexion aceptada desde %s: %d \n", inet_ntoa(cliente_addr.sin_addr ), ntohs ( cliente_addr.sin_port ));
    int bytes_recibidos = recv ( cliente_sockfd , msg , 256 , 0);
    printf ( "Tamano de mensaje : %d \n" , bytes_recibidos );

    char prueba[bytes_recibidos];
    prueba[0]='\0';
    for(int i = 0; i<bytes_recibidos; i++){
        prueba[i]=msg[i];
    }
    fflush(stdin);
    strtok(prueba, "\n");
    printf ( "Mensaje : %s \n\n" , prueba );


    char salida[256];
    char salidaReal[256];
    FILE *f = popen(prueba, "r");
    while (fgets(salida, 100, f) != NULL) {
        strcat( salidaReal, salida);
    }
    salida[0]='\0';
    pclose(f);
    fflush(stdin);

    if(salidaReal[0]=='\0'){
        strcat(salidaReal, "sh: 1: ");
        strcat(salidaReal, prueba);
        strcat(salidaReal, ": not found");
    }

    if(strcmp(prueba, "EOF") == 0){
      salir = 0;
      salidaReal[0] = '\0';
      strcat(salidaReal, "saliendo...");
    }

    prueba[0]='\0';

    //while(1){
      struct sockaddr_storage cliente;
      unsigned int addres_size = sizeof(cliente);
      int conectado = accept(sockfd, (struct sockaddr*) &cliente, &addres_size);
      if(conectado==-1){
        printf("NO se puede conectar socket secundario\n");
      }
      send(conectado, salidaReal, strlen(salidaReal), 0);
      salidaReal[0] = '\0';
    //}


    close ( cliente_sockfd );

  }
  return 0;
}
