/* Práctica 2 : Sockets (Redes de Computadoras)
   Alvarez Cerrillo Mijail Aron 310020590
   Este programa es el Cliente, el cual recibe un la direccion IPv4 como
   primer argumento y el numero de puerto como segundo argumento.
   El script servidor.c viene mejor documentado respecto a las librerias
   que se ocupan, se recomienda leer la documentacion de dicho script, para
   comprender de una mejor manera este archivo, asi como tambien el .md*/
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
/* Para poder usar write and read */
#include<unistd.h>

/* Funcion que imprime el error y termina con el programa */
void error(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  int sockfd, portno, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  char buffer[1000];
  if (argc < 3)
    error("Por favor ingrese una direccion y puerto como argumentos.");
  portno = atoi(argv[2]);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    error("Error abriendo el socket");
  server = gethostbyname(argv[1]);
  if (server == NULL)
    error("No existe tal servidor");
  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;

  /* void bcopy(const void *s1, void *s2, size_t n); 
     The bcopy() function shall copy n bytes from the area pointed to by s1 to
     the area pointed to by s2. The bytes are copied correctly even if the area
     pointed to by s1 overlaps the area pointed to by s2. */
  bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,
	server->h_length);
  serv_addr.sin_port = htons(portno);
  if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    error("Error al conectar");

  /* Ciclo en el que ingresamos un mensaje, el cual lo recibira el servidor y lo
     usara como comando con popen() */
  printf("Para terminar la conexión escribe 'EOF'\n");
  while(1){
    printf("->");
    memset(buffer, 0, 1000);
    fgets(buffer,1000,stdin);
    if(strcmp(buffer,"EOF\n") == 0)
      error("BYE");
    /* Mandar mensaje al servidor */
    n = write(sockfd, buffer, strlen(buffer));
    if (n < 0)
      error("Error al escribir en el socket");
    /* Lee el mensaje del servidor */
    memset(buffer, 0, 1000);
    n = read(sockfd, buffer, 1000);
    if (n < 0)
      error("Error al leer del socket");
    printf("%s",buffer);
  }
  
  return 0;
}
