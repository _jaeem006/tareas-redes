#include "lista.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void inserta(struct lista *l);
void lee(struct lista *l);
void elimina(struct lista *l);
void imprime(struct lista *l);

int main(){

  // Creamos la lista que vamos a usar
  struct lista l = {cabeza : 0, longitud : 0};

  // Selección del usuario
  int input;

  // Ciclo infinito
  for(;;){

    // Menú
    printf("******* Menú *******\n");
    printf("1. Insertar archivo\n");
    printf("2. Leer archivo\n");
    printf("3. Eliminar archivo\n");
    printf("4. Imprimir archivo\n");
    printf("5. Salir\n");
    printf("********************\n");

    // Leemos la selección del usuario
    scanf("%d", &input);

    fflush(stdin);

    switch(input){
      case 1:
        inserta(&l);
        break;
      case 2:
        lee(&l);
        break;
      case 3:
        elimina(&l);
        break;
      case 4:
        imprime(&l);
        break;
      case 5:
        return 0;
      default:
        printf("-------Ingresa un valor válido-------\n");
    }
  }
  return 0;
}

/* Función que nos dice si un archivo existe o no */
int fileExists(char fname[]){
  return access(fname, F_OK) != -1;
}

/* Función que inserta el nombre de un archivo en una lista */
void inserta(struct lista *l){

  char fname[51] = "";

  printf("Inserta el nombre del archivo. Máximo 50 caracteres.\n");

  scanf("%50s", fname);
  fflush(stdin);

  printf("-----------------------------\n");

  if(fileExists(fname)){

    // Creamos el apuntador que va a ser parte de la lista
    char *p = (char *)malloc(sizeof(char) * strlen(fname));

    strcpy(p, fname);

    agrega_elemento(l, p);

    printf("Archivo %s agregado correctamente \n", fname);

  }else
    // printf("El archivo %s no existe \n", fname);
    fprintf(stderr, "El archivo %s no existe \n", fname);

  printf("-----------------------------\n");
}

/* Función que lee un archivo que está en la lista */
void lee(struct lista *l){

  int input;

  printf("Inserta un número n y leeré el archivo en esa posición de mi lista.\n");

  scanf("%d", &input);
  fflush(stdin);

  printf("-------------Contenido-------------\n");

  if(l -> longitud < input){
    // printf("No tengo tantos archivos, sólo tengo %d\n", l -> longitud);
    fprintf(stderr, "No tengo tantos archivos, sólo tengo %d\n", l -> longitud);
  } else{
    for(int i = 0; i < l -> longitud; i++){
      if(i == input){

        char *fname = (char *)obten_elemento(l, input);

        FILE *fptr = fopen(fname, "r");

        if(fptr == NULL){
          fprintf(stderr, "No pude abrir el archivo %s\n", fname);
          return;
        }

        char c = fgetc(fptr);

        while(c != EOF){
          printf("%c", c);
          c = fgetc(fptr);
        }

        fclose(fptr);
      }
    }
  }

  printf("------------------------------------\n");
}

/* Función que elimina un archivo de una lista */
void elimina(struct lista *l){

  int input;

  printf("Inserta un número n y eliminaré el archivo en esa posición de mi lista.\n");

  scanf("%d", &input);
  fflush(stdin);

  printf("-----------------------------\n");

  if(l -> longitud < input){
    // printf("No tengo tantos archivos, sólo tengo %d\n", l -> longitud);
    fprintf(stderr, "No tengo tantos archivos, sólo tengo %d\n", l -> longitud);
  } else{
    elimina_elemento(l, input);
    printf("Elemento en la posición %d eliminado correctamente\n", input);
  }

  printf("-----------------------------\n");

}

/* Función que imprime una lista de archivos */
void imprime(struct lista *l){

  struct nodo *t = l -> cabeza;

  printf("---- Lista de archivos ----\n");
  for(int i = 0; i < l -> longitud; i++){

    printf("%d. %s\n", i, (char *)(t -> elemento));
    t = t -> siguiente;

  }
  printf("---------------------------\n");
}
